#!/usr/bin/env python3
import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode
import pprint

application = Flask(__name__)
app = application
pp = pprint.PrettyPrinter(indent=4)


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = (

        "CREATE TABLE movies("
        "id INT UNSIGNED NOT NULL AUTO_INCREMENT,"
        "title text NOT NULL,"
        "year text NOT NULL,"
        "director text NOT NULL,"
        "actor text NOT NULL,"
        "release_date text NOT NULL,"
        "rating DOUBLE PRECISION(11,2) NOT NULL,"
        "PRIMARY KEY(id))"
    )


    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None



@app.route("/")
def hello(msg="Chris'Hello World"):
#def hello():
#def hello(msg="Chris'Hello World"):

    print("Inside hello")
    print("Printing available environment variables")
    # print(os.environ)
    print("Before displaying index.html")
    msg = "testing - under construction"
    return render_template('index.html', message=msg)





#Insert/Update a Movie
@app.route('/add_movie', methods=['POST'])
def add_movie():
    #confirm thast the form exists
    response = request.form
    #get all form values
    title = response.get('title')

    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True) #needed buffered to hold all data

    # Check if the movie exist
    try:
        q = "SELECT title from movies WHERE title={0!r:}".format(title)
        print(q)
        cur.execute(q)
        cnx.commit()
    except mysql.connector.Error as err:
            pp.pprint(err.msg)
    
    movie_exist = cur.rowcount > 0
    if movie_exist:
        msg = "Movie {} not inserted - {}.".format(title, "the movie is already in the database")
        return render_template('index.html', message=msg)
    
    try:
        #insert the new movie
        sql_query = "INSERT INTO movies (title, year, director, actor, release_date, rating) "
        sql_values = "VALUES ('%(title)s', '%(year)s', '%(director)s', '%(actor)s', '%(release_date)s', '%(rating)s') " %(response)
        q = sql_query + sql_values
        print(q)
        cur.execute(q)
        cnx.commit()
    except mysql.connector.Error as err:
            pp.pprint(err.msg)
    
    message = "Movie {0} successfully inserted.".format(title)
    return render_template('index.html', message=message)

#Insert/Update a Movie
@app.route('/update_movie', methods=['POST'])
def update_movie():
    #confirm thast the form exists
    response = request.form
    #get all form values
    title = response.get('title')
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True) #needed buffered to hold all data

    # Check if the movie exist
    try:
        q = "SELECT title from movies WHERE title={0!r:}".format(title)
        print(q)
        cur.execute(q)
        cnx.commit()
    except mysql.connector.Error as err:
            pp.pprint(err.msg)
    
    movie_exist = cur.rowcount < 1
    if movie_exist:
        message = "Movie {} not updated - {}.".format(title, "the movie is not in the database")
        return render_template('index.html', message=message)
    
    try:
        #insert the new movie
        q1 = 'UPDATE movies '
        q2 = 'SET ' + ', '.join("{!s}={!r}".format(k, v) for (k, v) in response.items() if k != 'title')
        q3 = ' WHERE title={0!r:}'.format(title)
        q = q1 + q2 + q3
        print(q)
        cur.execute(q)
        cnx.commit()
    except mysql.connector.Error as err:
            pp.pprint(err.msg)
    
    message = "Movie {0} successfully inserted.".format(title)
    return render_template('index.html', message=message)

#Delete a Movie
@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    #confirm thast the form exists
    response = request.form
    #get all form values
    title = response.get('title')

    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True) #needed buffered to hold all data

    # Check if the movie exist
    try:
        q = "SELECT title from movies WHERE title={0!r:}".format(title)
        print(q)
        cur.execute(q)
        cnx.commit()
    except mysql.connector.Error as err:
            pp.pprint(err.msg)
    
    movie_exist = cur.rowcount < 1
    if movie_exist:
        msg = "Movie {} not deleted - {}.".format(title, "the movie doesn't exist")
        return render_template('index.html', message=msg)
    
    try:
        #insert the new movie
        q = "DELETE FROM movies WHERE title={0!r:}".format(title)
        print(q)
        cur.execute(q)
        cnx.commit()
    except mysql.connector.Error as err:
            pp.pprint(err.msg)
    
    message = "Movie {0} successfully deleted.".format(title)
    return render_template('index.html', message=message)


#Search Movies with given actor
@app.route('/search_movie', methods=['GET'])
def search_movie():
    #confirm thast the form exists
    response = request.args
    #get all form values
    actor = response.get('actor')

    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True) #needed buffered to hold all data

    # Check if the movie exist
    try:
        q = "SELECT * from movies WHERE actor={0!r:}".format(actor)
        print(q)
        cur.execute(q)
        cnx.commit()
    except mysql.connector.Error as err:
            pp.pprint(err.msg)
    
    movie_exist = cur.rowcount < 1
    if movie_exist:
        message = "No movies with actor {} found - {}.".format(actor, "no records in the database")
        return render_template('index.html', message=message)
    
    results = [list(row) for row in cur.fetchall()]
    
    results_trim = list()
    for row in results:
        line = ', '.join((row[1], row[2], row[4])) + '.'
        results_trim.append(line)
    
    message = "Movies with actor {0} successfully listed.".format(actor)
    return render_template('index.html', message=message, results=results_trim)

#Print Movie statistics
@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True) #needed buffered to hold all data

    # Check if the movie exist
    try:
        q = "SELECT * FROM movies WHERE rating = (SELECT MIN(rating) from movies) "
        print(q)
        cur.execute(q)
        cnx.commit()
    except mysql.connector.Error as err:
            pp.pprint(err.msg)
    
    movie_exist = cur.rowcount < 1
    if movie_exist:
        message = "No movies found - {}.".format("no records in the database")
        return render_template('index.html', message=message)
    
    results = [list(row) for row in cur.fetchall()]
    
    results_trim = list()
    for row in results:
        line = ', '.join((row[1], row[2], row[4], row[3], str(row[6]))) + '.'
        results_trim.append(line)
    pp.pprint(results_trim)
    message = "Movies with lowest rating successfully listed."
    return render_template('index.html', message=message, results=results_trim)

    
#Print Movie statistics
@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor(buffered=True) #needed buffered to hold all data

    # Check if the movie exist
    try:
        q = "SELECT * FROM movies WHERE rating = (SELECT MAX(rating) from movies) "
        print(q)
        cur.execute(q)
        cnx.commit()
    except mysql.connector.Error as err:
            pp.pprint(err.msg)
    
    movie_exist = cur.rowcount < 1
    if movie_exist:
        message = "No movies found - {}.".format("no records in the database")
        return render_template('index.html', message=message)
    
    results = [list(row) for row in cur.fetchall()]
    
    results_trim = list()
    for row in results:
        line = ', '.join((row[1], row[2], row[4], row[3], str(row[6]))) + '.'
        results_trim.append(line)
    pp.pprint(results_trim)
    message = "Movies with highest rating successfully listed."
    return render_template('index.html', message=message, results=results_trim)

# Method Not Allowed

# The method is not allowed for the requested URL.



if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
