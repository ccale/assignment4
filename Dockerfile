FROM ubuntu:14.04

ENV PASSWORD=testpass123!@#
ENV DB=testdb
ENV HOST=35.239.36.251 
ENV USER=root

RUN apt-get update -y \ 
    && apt-get install -y python3-setuptools python3-pip python-mysqldb

ADD requirements.txt /src/requirements.txt
RUN cd /src; pip3 install -r requirements.txt
ADD . /src
EXPOSE 5000
CMD ["python3", "/src/movies_web_app.py"]